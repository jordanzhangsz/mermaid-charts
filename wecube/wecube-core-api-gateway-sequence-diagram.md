
API Gateway Sequence Diagram

```mermaid
sequenceDiagram
    participant Front as Web Front <br/>(Browser)
    participant Core as Wecube Core <br/>(Web Server) <br/> (e.g. 10.0.0.1)
    participant PluginQ as Plugin<br/>(Restful API Server) <br/>(e.g. qcloud 10.0.0.2)
    participant PluginS as Plugin<br/>(Restful API Server) <br/>(e.g. saltstack 10.0.0.3)

    Front ->>+ Core: HTTP Request http://10.0.0.1/api-proxy/qcloud/api/v1/vms
    Note right of Core: 1, Routing <br/> 2, Access Control<br/> 3, Forwarding
    Core->>+PluginQ : HTTP Request http://10.0.0.2/api/v1/vms
    PluginQ -->>-Core: HTTP Response
    Core-->>-Front: HTTP Respose

        Front ->>+ Core: HTTP Request http://10.0.0.1/api-proxy/saltstack/api/v1/log
    Note right of Core: 1, Routing <br/> 2, Access Control<br/> 3, Forwarding
    Core->>+PluginS : HTTP Request http://10.0.0.3/api/v1/log
    PluginS -->>-Core: HTTP Response
    Core-->>-Front: HTTP Respose

```